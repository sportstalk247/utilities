#!/usr/bin/env node
const sdk = require('sportstalk-sdk');


// Read command line parameters
const args = require('yargs')
    .usage('Usage: $0 --appid=<YOUR-APPID> --token=<YOUR-TOKEN> --roomid=<ROOMID> --endpoint=<ENDPOINT>')
    .describe('appid', 'APPID from dasboard')
    .describe('token', 'APP TOKEN from dashboard')
    .describe('roomid', 'ROOM ID of the room to post.')
    .describe('userid', 'userid of user to enter room with')
    .describe('handle', 'handle of user to enter the room with')
    .describe('delay', 'time between messages')
    .describe('messages', 'number of events to send')
    .describe('displayname', 'The display name of the user to enter the room')
    .demandOption(['appid','token','userid'])
    .default('handle', 'tailbot')
    .default('messages', 5)
    .default('displayname', 'chattailbot')
    .default('userid', 'tailbot').argv;

const gl_token = args.token;
const gl_appid = args.appid;
const gl_roomid = args.roomid;
const gl_stendpoint = args.endpoint;
const gl_messages = parseInt(args.messages);
const delay = parseInt(args.delay) || 10;


let handle = args.handle || 'tailbot' ;
let userid = args.userid || 'tailbot';
let displayname = args.displayname || 'chattailbot';

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

// When we join a room we get the recent event history for the room
var catchupOnJoin = async function(events) {
    for(let loop = 0; loop < events.length; loop++) {
        // Process event
        onHandleEvent(events[loop]);
    }
}

var go = async function() {
    // Initialize SportsTalk247 Chat Client
    const config = { appId : gl_appid, apiToken: gl_token }
    if(gl_stendpoint) {
        config.endpoint=gl_stendpoint
    }
    console.log(config);
    const chatClient = sdk.ChatClient.init(config);
    // Define the user we want to use
    chatClient.setUser({ userid: userid, handle: handle, displayname: displayname });
    chatClient.setEventHandlers({onChatEvent:()=>{}})
    let joinRoomPromise;
    if(!gl_roomid && !gl_roomcustomid) {
        console.error("INVALID SETTINGS: Must set either ROOMID ");
        return;
    }

    joinRoomPromise = chatClient.joinRoom(gl_roomid);
    // Join the room
    joinRoomPromise.then(async function(roomDetailsAndUpdates){
        for(let i=0; i<gl_messages; i++) {
            const message = "testing "+i+" timestamp: "+new Date().toDateString();
            console.log(message);
            chatClient.executeChatCommand(message).catch(e=>{
                console.log(e);
            })
            await sleep(delay)
        }
    }).catch(function(result) {
        if(result.response && result.response.data) {
            console.error(`${result.response.data.code} | ${result.response.data.message}`);
        }else  {
            console.error(result);
        }
    });
}

go();