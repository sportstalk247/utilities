#!/usr/bin/env node
const sdk = require('sportstalk-sdk');

// Read command line parameters
const args = require('yargs')
    .usage('Usage: $0 --appid=<YOUR-APPID> --token=<YOUR-TOKEN> --roomcustomid=<ROOM-CUSTOMID> --roomid=<ROOMID>')
    .describe('appid', 'APPID from dasboard')
    .describe('token', 'APP TOKEN from dashboard')
    .describe('roomid', 'ROOM ID of the room to tail.')
    .describe('roomcustomid', 'CUSTOMID of room to tail. Will override roomid if both are set.')
    .describe('userid', 'userid of user to enter room with')
    .describe('handle', 'handle of user to enter the room with')
    .describe('displayname', 'The display name of the user to enter the room')
    .demandOption(['appid','token','userid'])
    .default('handle', 'tailbot')
    .default('displayname', 'chattailbot')
    .default('userid', 'tailbot').argv;

const gl_token = args.token;
const gl_appid = args.appid;
const gl_roomcustomid = args.roomcustomid;
const gl_roomid = args.roomid;
const gl_stendpoint = args.endpoint;


let handle = args.handle || 'tailbot' ;
let userid = args.userid || 'tailbot';
let displayname = args.displayname || 'chattailbot';

// Our event handler
var onHandleEvent = async function(ev) {
    switch (ev.eventtype) {
        case "speech":
        case "quote":
        case "reply":
            console.log(`(${ev.eventtype}) ${ev.user.handle}: ${ev.body}`);
            break;
        case "action":
            console.log(`(${ev.eventtype}) ${ev.user.handle} ${ev.body}`);
            break;
        case "replace":
            console.log(`(${ev.eventtype}) [id=${ev.id} parentid=${ev.parentid}]:`);
            console.log(ev);
            break;
        default: {
            console.log(`(${ev.eventtype}) [id=${ev.id} parentid=${ev.parentid}]`);
        }
    }
}

// When we join a room we get the recent event history for the room
var catchupOnJoin = async function(events) {
    for(let loop = 0; loop < events.length; loop++) {
        // Process event
        onHandleEvent(events[loop]);
    }
}

var go = async function() {
    // Initialize SportsTalk247 Chat Client
    const config = { appId : gl_appid, apiToken: gl_token }
    if(gl_stendpoint) {
        config.endpoint=gl_stendpoint
    }
    const chatClient = sdk.ChatClient.init(config);

    // Register event handlers
    chatClient.setEventHandlers({
        onChatEvent: onHandleEvent
    })

    // Define the user we want to use
    chatClient.setUser({ userid: userid, handle: handle, displayname: displayname });
    let joinRoomPromise;
    if(!gl_roomid && !gl_roomcustomid) {
        console.error("INVALID SETTINGS: Must set either a ROOMID or a ROOM CUSTOM ID");
        return;
    }
    if(gl_roomcustomid) {
        joinRoomPromise = chatClient.joinRoomByCustomId(gl_roomcustomid)
    } else {
        joinRoomPromise = chatClient.joinRoom(gl_roomid);
    }
    // Join the room
    joinRoomPromise.then(function(roomDetailsAndUpdates){
        // The response will include room details and also a list of recent chat events.
        // Non-displayable event types will not be in the list (purge,remove,replace,react) for example
        // JOIN will automatically call your events handler for each of the events in the above list
        // JOIN will set the cursor for listening to future events in the stream, starting when you start listening.
        console.log(`Joined room ${roomDetailsAndUpdates.room.name} as @${handle}`);

        // Start listening for updates
        chatClient.startListeningToEventUpdates();
    }).catch(function(result) {
        if(result.response && result.response.data) {
            console.error(`${result.response.data.code} | ${result.response.data.message}`);
        }else  {
            console.error(result);
        }
    });
}

go();